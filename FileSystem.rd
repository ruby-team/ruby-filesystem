=begin

= FileSystem


*Version: 0.03
*Date: 2002-05-02
*Author: Mike Hall
*e-mail: mghall@enteract.com
*Home Page: ((<URL:http://www.enteract.com/~mghall/ruby>))

== Preface
This is a simple extension to get file-system information
from the operating system.  It uses the 'statvfs' and 'getmntent'
routines on Linux and Solaris.

== Module Description
* FileSystem
  * FileSystem.mounts

Several calling formats:

	FileSystem.mounts
		returns array describing currenly mounted filesystem

	FileSystem.mounts "filename" 
		returns array of filesystem descriptions from given file

	FileSystem.mounts {|m| block }
		invokes block for each mounted filesystem

	FileSystem.mounts("filename") {|m| block}
		invokes block for each filesystem described in the file
		
The default mount-table filename is "/etc/mtab" on Linux,
and "/etc/mnttab" on Solaris.  
On Linux, FileSystem.mounts can be used to scan "/etc/fstab" also.

  * FileSystemMount Structure
    * m.device        file system (i.e. partition device) name
    * m.mount         mount point directory
    * m.fstype        file system type
    * m.options       mount options
    * m.time          time the filesystem was mounted (Solaris)
    * m.dump_interval dump frequency in days (Linux/BSD)
    * m.check_pass    pass number of file system check (Linux/BSD)

  * FileSystem.stat

	One invocation:

	FileSystem.stat "path"
		returns structure describing the designated filesystem

  * FileSystemStat Structure
    * s.path          directory path provided
    * s.block_size    optimal transfer block size
    * s.blocks        total number of data blocks in file system
    * s.blocks_free   number of free blocks in file system
    * s.blocks_avail  number of free blocks available to non-super-user
    * s.files         total number of file nodes in file system
    * s.files_free    number of free file nodes in file system
    * s.files_avail   number of free file nodes available to non-super-user
    * s.flags         file system flags
    * s.maxnamelen    maximum file name length

* Constants for FileSystemStat.flags
  * Solaris: RDONLY NOSUID NOTRUNC
  * Linux: RDONLY NOSUID
  * GNU: RDONLY NOSUID NODEV NOEXE SYNC MANDLOCK WRITE APPEND IMMUTABLE 
    NOATIME NODIRATIME

== Installation
(1) Unpack this archive
(1) ruby extconf.rb
(1) make
(1) ruby Test.rb
(1) ruby dfrb
(1) make install

== Usage and Samples

Simple usage:

    # get an array of mounted filesystems
    mts = FileSystem.mounts

    # get the status of one filesystem
    s = FileSystem.stat '/tmp'

A larger example, simulating the 'df' command:

    require 'filesystem'
    
    def pct(total, avail)
      (total == 0) ? 0 : (100.0 * avail / total).round
    end
    
    FMT = "%-10s %8d %8d  %3d   %8d %8d  %3d"
    HDR = "Mount            KB    Avail %free     Files    Avail %free"
    
    def df(mnt)
      puts HDR if mnt.size > 0
      mnt.sort.each do |m|
        s = FileSystem.stat m
        bpct = pct(s.blocks, s.blocks_avail)
        fpct = pct(s.files,  s.files_avail)
        puts format(FMT, m, s.blocks, s.blocks_avail, bpct, 
                            s.files,  s.files_avail,  fpct)
      end
    end
    
    def mounted
      mts = []
      FileSystem.mounts.each {|m| mts << m.mount if m.device =~ %r(/dev/)}
      mts
    end
    
    df ((ARGV.size == 0) ? mounted : ARGV)


== Documentation
* The README file
* The 'FileSystem.html' file
* This RDOC file, 'FileSystem.rd'
* The 'FileSystem.ri' file is a source description for the 'ri' command.
  Copy if to your 'ri/srcdesc' source directory as 'FileSystem.rb',
  and regenerate the binary description files that 'ri' uses.
  See the 'ri' 'README' file for details.

== To-Do
* Convert 'st.flags' from a number to an array of strings
* Get patches/fixes for other UNIX systems

== Acknowledgements
* Daniel Berger for testing on Solaris, and motivation.
* comp.lang.ruby for suggesting better names.

== Reference
* 'statvfs' system call
* 'getmntent' library routine

=end
