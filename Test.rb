
require 'filesystem'

ARGV.push '/tmp' if ARGV.empty?

ARGV.each {|d|
  s = FileSystem.stat d
  puts "#{s.path}	#{s.blocks_avail}"
}

begin
  bad = FileSystem.stat '/nono'
rescue  
  puts "Got error with '/nono', good!"
end

puts "Mounted FileSystems:"
mts = FileSystem.mounts
mts.each {|m| puts "#{m.device}	#{m.mount}	#{m.fstype}\n" }

puts "Available FileSystems:"
['/etc/fstab', '/etc/vfstab', '/etc/mnttab'].each do |fst|
  if FileTest.exist?(fst)
    puts "In #{fst}:"
    FileSystem.mounts(fst) {|m| puts m.mount}
    break
  end
end

