SHELL = /bin/sh

#### Start of system configuration section. ####

srcdir = .
topdir = $(rubylibdir)/$(arch)
hdrdir = $(rubylibdir)/$(arch)
VPATH = $(srcdir)

CC = gcc

CFLAGS   = -fPIC -g -O2 
CPPFLAGS = -I. -I$(hdrdir) -I$(srcdir)   
CXXFLAGS = $(CFLAGS)
DLDFLAGS =  -L$(rubylibdir)/$(arch) -L$(exec_prefix)/lib 
LDSHARED = gcc -shared 

LIBPATH = 

RUBY_INSTALL_NAME = ruby
RUBY_SO_NAME = 
arch = i686-linux
ruby_version = 1.7
prefix = $(DESTDIR)/usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
sitelibdir = $(sitedir)/$(ruby_version)
datadir = $(prefix)/share
sitedir = $(prefix)/lib/ruby/site_ruby
sharedstatedir = $(prefix)/com
archdir = $(rubylibdir)/$(arch)
localstatedir = $(prefix)/var
infodir = $(prefix)/info
oldincludedir = $(DESTDIR)/usr/include
libexecdir = $(exec_prefix)/libexec
compile_dir = $(DESTDIR)/opt/ruby/misc/ruby
sbindir = $(exec_prefix)/sbin
includedir = $(prefix)/include
sysconfdir = $(prefix)/etc
sitearchdir = $(sitelibdir)/$(arch)
mandir = $(prefix)/man
libdir = $(exec_prefix)/lib
rubylibdir = $(libdir)/ruby/$(ruby_version)
target_prefix = 

#### End of system configuration section. ####

LOCAL_LIBS =  
LIBS = $(LIBRUBY_A) -lc
OBJS = filesystem.o

TARGET = filesystem
DLLIB = $(TARGET).so

RUBY = ruby
RM = $(RUBY) -rftools -e "File::rm_f(*ARGV.map do|x|Dir[x]end.flatten.uniq)"
MAKEDIRS = $(RUBY) -r ftools -e 'File::makedirs(*ARGV)'
INSTALL_PROG = $(RUBY) -r ftools -e 'File::install(ARGV[0], ARGV[1], 0555, true)'
INSTALL_DATA = $(RUBY) -r ftools -e 'File::install(ARGV[0], ARGV[1], 0644, true)'

EXEEXT = 

all:		$(DLLIB)

clean:
		@$(RM) *.o *.so *.sl *.a $(DLLIB)
		@$(RM) $(TARGET).lib $(TARGET).exp $(TARGET).ilk *.pdb $(CLEANFILES)
                
distclean:	clean
		@$(RM) Makefile extconf.h conftest.* mkmf.log
		@$(RM) core ruby$(EXEEXT) *~ $(DISTCLEANFILES)

realclean:	distclean

install:	$(archdir)$(target_prefix)/$(DLLIB)

site-install:	$(sitearchdir)$(target_prefix)/$(DLLIB)

$(archdir)$(target_prefix)/$(DLLIB): $(DLLIB)
	@$(MAKEDIRS) $(rubylibdir) $(archdir)$(target_prefix)
	@$(INSTALL_PROG) $(DLLIB) $(archdir)$(target_prefix)/$(DLLIB)

$(sitearchdir)$(target_prefix)/$(DLLIB): $(DLLIB)
	@$(MAKEDIRS) $(sitearchdir)$(target_prefix)
	@$(INSTALL_PROG) $(DLLIB) $(sitearchdir)$(target_prefix)/$(DLLIB)

install:

site-install:

.cc.o:
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $<
.cpp.o:
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $<
.cxx.o:
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $<
.C.o:
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $<
.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<
$(DLLIB): $(OBJS)
	$(LDSHARED) $(DLDFLAGS) -o $(DLLIB) $(OBJS) $(LIBS) $(LOCAL_LIBS)
